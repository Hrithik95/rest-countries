import React, { useContext } from "react";
import ModeContext from "./context/ModeContext";
import "./Mode.css";

export default function ModeChange() {
    const { toggleMode, ToggleMode } = useContext(ModeContext);
    const containerClass = toggleMode ? "dark-mode" : "light-mode";

    return (
        <div className={containerClass}>
            <div className="mode-container">
                <h3><b>Where in the world?</b></h3>
                <div className="mode-change">
                    <i className="dark-mode-icon fa-regular fa-moon" onClick={ToggleMode}></i>
                    <p>{toggleMode ? "Light Mode" : "Dark Mode"}</p>
                </div>
            </div>
        </div>
    );
}
