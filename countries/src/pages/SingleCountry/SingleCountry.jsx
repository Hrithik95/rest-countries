import React, { useEffect, useState, useContext } from "react";
import ModeChange from "../../Mode";
import { useParams, useNavigate } from "react-router-dom";
import ModeContext from "../../context/ModeContext";
import Loader from "../Loader/Loader";
import ErrorPage from "../Error";
import './SingleCountry.css';

export default function SingleCountry() {
    const [singleCountry, setSingleCountry] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);
    let params = useParams();
    let navigate = useNavigate();
    useEffect(() => {
        fetch(`https://restcountries.com/v3.1/name/${params.name}/?fullText=true`)
            .then((response) => {
                return response.json();
            }).then((data) => {
                setLoading(false);
                setSingleCountry(data);
            }).catch((error) => {
                setLoading(false);
                setError(true);
            })
    }, []);



    function navigateToHome() {
        navigate('/');
    }
    const { toggleMode } = useContext(ModeContext);
    return (
        <div className={` ${toggleMode ? "dark-mode" : "light-mode"}`}>
            {loading && <Loader />}
            {!loading && !error && <>
                <ModeChange />
                <input id="go-back-btn" type="button" value="Go Back" onClick={navigateToHome} />
                <div className="single-country">
                    {
                        singleCountry.map((element) => {
                            return <div className="country-container">
                                <div className="single-img">
                                    <img src={element.flags.png} />
                                </div>
                                <div className="single-country-detail">
                                    <h2>{element.name.common}</h2>
                                    <div className="main-div">
                                        <div className="div-one">
                                            <div><b>Native Name:</b> {Object.values(element.name.nativeName).map(
                                                (nativeName) => {
                                                    return <span>
                                                        {nativeName.common}&nbsp;
                                                    </span>
                                                }
                                            )}
                                            </div>
                                            <div>
                                                <b>Population:</b> {element.population}
                                            </div>
                                            <div>
                                                <b>Region:</b> {element.region}
                                            </div>
                                            <div>
                                                <b>Sub-Region:</b> {element.subregion}
                                            </div>
                                            <div>
                                                <b>Capital:</b> {element.capital}
                                            </div>
                                        </div>
                                        <div className="div-two">
                                            <div><b>Top Level Domain:</b> {element.tld[0]}</div>
                                            <div><b>Currencies:</b> {Object.values(element.currencies).map((currency) => {
                                                return <span>
                                                    {currency.name}&nbsp;
                                                </span>
                                            })}
                                            </div>
                                            <div>
                                                <b>Languages:</b> {Object.values(element.languages).map((lang) => {
                                                    return <span>
                                                        {lang}&nbsp;
                                                    </span>
                                                })}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="border-countries">
                                        {element.borders && <><b>Border Countries:</b> {element.borders && element.borders.length > 0 && Object.values(element.borders).map((border) => {
                                            return <span className="border-country-name">
                                                {border}&nbsp;
                                            </span>
                                        })}
                                        </>}
                                    </div>

                                </div>
                            </div>
                        })
                    }
                </div>
            </>}
            {error && <ErrorPage />}
        </div>
    )
}