import React, { useState, useContext } from "react"
import ModeContext from "../context/ModeContext";
import { useNavigate } from "react-router-dom";
import './Countries.css'


export default function Countries(props) {
    const { data } = props;
    const [sortedData, setSortedData] = useState("");
    const sortingData = [...data];
    const navigate = useNavigate();
    const { toggleMode } = useContext(ModeContext);
    const sortedAreaHandler = (event) => {
        console.log("inside area handler");
        setSortedData(event.target.value);
    }
    if (sortedData === "Ascending Area") {
        sortingData.sort((prevCountry, currCountry) => {
            if (prevCountry.area > currCountry.area) {
                return 1;
            } else if (prevCountry.area < currCountry.area) {
                return -1
            } else {
                return 0;
            }
        })
    }
    else if (sortedData === "Descending Area") {
        sortingData.sort((prevCountry, currCountry) => {
            if (prevCountry.area < currCountry.area) {
                return 1;
            } else if (prevCountry.area > currCountry.area) {
                return -1
            } else {
                return 0;
            }
        })
    }

    const sortedDataHandler = (event) => {
        console.log("inside population handler");
        setSortedData(event.target.value)
    }
    if (sortedData === "Ascending Population") {
        sortingData.sort((prevCountry, currCountry) => {
            if (prevCountry.population > currCountry.population) {
                return 1;
            } else if (prevCountry.population < currCountry.population) {
                return -1
            } else {
                return 0;
            }
        })
    }
    else if (sortedData === "Descending Population") {
        sortingData.sort((prevCountry, currCountry) => {
            if (prevCountry.population < currCountry.population) {
                return 1;
            } else if (prevCountry.population > currCountry.population) {
                return -1
            } else {
                return 0;
            }
        })
    }

    function navigateToSingleCountry(name) {
        navigate(`/country/${name}`);
    }
    return (
        <>
            <div className={toggleMode ? "dark-mode" : "light-mode"}>

                <div className="container">
                    <div className="dropdown">
                        <select onClick={sortedDataHandler}>
                            <option >Sort By Population</option>
                            <option >Ascending Population</option>
                            <option >Descending Population</option>
                        </select>
                        <select onClick={sortedAreaHandler}>
                            <option >Sort By Area</option>
                            <option >Ascending Area</option>
                            <option >Descending Area</option>
                        </select>
                    </div>
                    <div className="main-container">
                        {
                            sortingData.length === 0 ? (
                                <h1 className="no-item">No Country to display</h1>
                            ) : (

                                sortingData.map((countryData) => {
                                    return <div className="country-details" onClick={() => navigateToSingleCountry(countryData.name.common)}>
                                        <div className="image-container">
                                            <img className="image" src={countryData.flags.png} />
                                        </div>
                                        <div className="country-detail">
                                            <p className="country-name">{countryData.name.common}</p>
                                            <p className="country-population"><b>Population</b>: {countryData.population}</p>
                                            <div className="country-region"><b>Region</b>: {countryData.region}</div>
                                            <div className="country-capital"><b>Capital</b>: {countryData.capital}</div>
                                            <div className="country-capital"><b>Area</b>: {countryData.area}</div>
                                            <div className="subregion"><b>SubRegion</b>: {countryData.subregion}</div>
                                        </div>
                                    </div>
                                })

                            )
                        }

                    </div>
                </div>
            </div>

        </>
    )

}