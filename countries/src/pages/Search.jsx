import React, { useContext } from "react"
import ModeContext from "../context/ModeContext"
import './Search.css'

export default function Search(props) {
    const { regions, filterByRegion, filterBySearch } = props
    const { toggleMode } = useContext(ModeContext);
    return (
        <div className={` ${toggleMode ? "dark-mode" : "light-mode"}`}>
            <div className="search-container">
                <div className="search-box">
                    <i class="search-icon fa-solid fa-magnifying-glass"></i>
                    <input type="text" placeholder="Search for a country" onChange={(event) => filterBySearch(event.target.value)} />
                </div>
                <div className="search-dropdown">
                    <select onClick={(event) => filterByRegion(event.target.value)}>
                        {
                            regions.map((region) => {
                                return <option id={region} >{region}</option>
                            })
                        }
                    </select>
                </div>
            </div>
        </div>
    )
}