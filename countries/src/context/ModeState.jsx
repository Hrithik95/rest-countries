import ModeContext from "./ModeContext";
import { useState } from "react";

const ModeState = (props) => {

    const [toggleMode, setToggleMode] = useState(false);

    function ToggleMode() {
        console.log(toggleMode);
        setToggleMode(!toggleMode);
    }

    return (
        <ModeContext.Provider value={{ toggleMode, ToggleMode }}>
            {props.children}
        </ModeContext.Provider>
    )
}
export default ModeState;

