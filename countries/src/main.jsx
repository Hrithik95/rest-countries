import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import ModeState from './context/ModeState'
import SingleCountry from './pages/SingleCountry/SingleCountry';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import './index.css'

const router = createBrowserRouter([
  {
    path: '/',
    element: <App />
  },
  {
    path: '/country/:name',
    element: <SingleCountry />
  }
])

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <ModeState>
    <RouterProvider router={router} />
    </ModeState>
  </React.StrictMode>,
)
