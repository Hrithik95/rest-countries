import React, { useState, useEffect, useContext } from 'react'
import Countries from './pages/Countries';
import ModeChange from './Mode';
import Search from './pages/Search';
import ErrorPage from './pages/Error';
import ModeContext from "./context/ModeContext"
import Loader from './pages/Loader/Loader';
import './App.css'

const REST_API = 'https://restcountries.com/v3.1/all';

function App() {


  const [data, setData] = useState([]);
  const [regionsArray, setRegionsArray] = useState([]);
  const [searchQuery, setSearchQuery] = useState("");
  const [selectRegion, setSelectRegion] = useState("All Region");
  const [selectSubRegion, setSelectSubRegion] = useState("All Sub-region");
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const { toggleMode } = useContext(ModeContext);


  function filterBySearch(countryName) {
    console.log("hello")
    setSearchQuery(countryName);
  }

  function filterByRegion(region) {
    console.log("hello")
    setSelectRegion(region);
    setSelectSubRegion("All Sub-region");
  }

  function filterBySubRegion(event) {
    setSelectSubRegion(event.target.value);
  }


  const countriesData = data.filter((country) => {
    const nameMatch = country.name.common.toLowerCase().includes(searchQuery.toLowerCase()) || searchQuery === "";
    const regionMatch = selectRegion === "All Region" || country.region.toLowerCase() === selectRegion.toLowerCase();
    const subRegionMatch = selectSubRegion === "All Sub-region" || (selectSubRegion === "No Sub-region" && !country.subregion) || (country.subregion && country.subregion.toLowerCase() === selectSubRegion.toLowerCase());
    return nameMatch && regionMatch && subRegionMatch;
  });
  console.log(countriesData);
  const uniqueSubregions = Array.from(new Set(countriesData.map((country) => {
    return country.subregion || "No Sub-region";
  })));

  const subRegionOptions = uniqueSubregions.map((subregion) => {
    return (
      <option key={subregion} value={subregion}>
        {subregion}
      </option>
    )
  });
  // console.log(subRegionOptions);

  useEffect(() => {
    fetch(REST_API).then((response) => {
      return response.json();
    }).then((result) => {
      const uniqueRegions = [...new Set(result.map((regions) => {
        return regions.region;
      }))];
      setData(result);
      setLoading(false);
      setRegionsArray(uniqueRegions);

    }).catch((error) => {
      console.error(" inside catch error");
      setLoading(false);
      setError(true);
    })
  }, [])

  return (
    <div className={`app-container ${toggleMode ? "dark-mode" : "light-mode"}`}>
      {loading && <Loader />}
      {!loading && !error && <>
        <ModeChange />
        <div className='search-and-select'>
          <Search regions={["All Region", ...regionsArray]} filterByRegion={filterByRegion} filterBySearch={filterBySearch} />
          {selectRegion === "All Region" ? <select className='search' name="" id="" onChange={filterBySubRegion} value={selectSubRegion}>
            <option value="All Sub-region">All Sub-region</option>
          </select> : <select className='search' name="" id="" onChange={filterBySubRegion} value={selectSubRegion}>
            <option value="All Sub-region">All Sub-region</option>
            {subRegionOptions}
          </select>}

        </div>
        <Countries data={countriesData} />
      </>}
      {error && <ErrorPage />}
    </div>
  )
}

export default App
